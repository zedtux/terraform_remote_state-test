# `terraform_remote_state` bug with gitlab.com

This repository reproduces a bug with Terraform remote state using Gitlab Terraform remote state feature.

## Usage

1. Initialize terraform:
```
export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
export TF_STATE_NAME=default
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/53992830/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/53992830/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/53992830/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=zedtux" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```
2. Create a `.auto.tfvars` file with:
```
gitlab_project_id = "53992830"
gitlab_username = "<YOUR-USERNAME>"
gitlab_access_token = "<YOUR-ACCESS-TOKEN>"
```
3. Run `terraform plan`:
```
data.terraform_remote_state.default: Reading...

Planning failed. Terraform encountered an error while generating this plan.

╷
│ Error: Unable to find remote state
│
│   with data.terraform_remote_state.default,
│   on remote_state.tf line 1, in data "terraform_remote_state" "default":
│    1: data "terraform_remote_state" "default" {
│
│ No stored state was found for the given workspace in the given backend.
╵
```
